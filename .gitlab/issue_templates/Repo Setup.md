This issue documents the progress of applying a repository best practices.

Initial Setup
-------------

- [ ] **README.md**
  - [ ] Logo
  - [ ] Title
  - [ ] Description
  - [ ] Shields
  - [ ] 🧐 About
  - [ ] 🏁 Getting Started
  - [ ] 🚀 Deployment
  - [ ] 🤹 Usage
  - [ ] 📜 Changelog
  - [ ] 🗺️  Roadmp
  - [ ] ✅ Todo
  - [ ] 🔨 Built Using
  - [ ] ⚙️  Contributing
  - [ ] ✍️  Credits
  - [ ] 🤲 Support
  - [ ] ⚖️  License
- [ ] **LICENSE.md**
- [ ] **CONTRIBUTING.md**
- [ ] **CHANGELOG.md**

Documentation
--------------

*TODO*

CI/CD
-----

*TODO*
