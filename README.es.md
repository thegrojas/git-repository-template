<div align="center">
<a href="" rel="noopener">

<img width=200px height=200px src="https://i.imgur.com/6wj0hh6.jpg" alt="Logo del proyecto"></a>

# Título del proyecto

*Sean cuales sean tus planes, el éxito de tu día comienza ahora. Respira hondo y comienza con entusiasmo.*

[![Estado](https://img.shields.io/badge/estado-activo-success.svg)]() 
[![Licencia](https://img.shields.io/badge/licencia-MIT-blue.svg)](./LICENSE)

---

<a href="#acerca_de">🧐 Acerca de</a> •
<a href="#introduccion">🏁 Introducción</a> •
<a href="#despliegue">🚀 Despliegue</a> •
<a href="#uso">🤹 Uso</a> •
<a href="#registro_cambios">📜 Registro de cambios</a> •
<a href="#plan_desarrollo">🗺️ Plan de desarrollo</a> •
<a href="#pendientes">✅ Pendientes</a> •
<a href="#tecnologias_utilizadas">🔨 Tecnologías utilizadas</a> •
<a href="#contribuciones">⚙️ Contribuciones</a> •
<a href="#creditos">✍️ Créditos</a> •
<a href="#soporte">🤲 Soporte</a> •
<a href="#licencia">⚖️ Licencia</a>

![Captura de pantalla](https://via.placeholder.com/720x360.png?text=Captura+de+pantalla+para+demostrar+el+proyecto)

</div>

## 🧐 Acerca de <a name="acerca_de"></a>

Escriba uno o dos párrafos describiendo el propósito del proyecto.

## 🏁 Introducción <a name="introduccion"></a>

Las instrucciones a continuación le permitirán tener una copia del proyecto para su desarrollo y pruebas. Consulte la sección [despliegue](#despliegue) para más información sobre como desplegar este proyecto en un sistema de producción.

### 🦺 Requerimientos previos

Antes de proceder, los siguientes requerimientos deben ser satisfechos.

```
- Lista de requerimientos
```

### 📦 Instalación

Para proceder con la instalación se deben seguir los siguientes pasos:

1. Asegúrese de que los requerimientos mencionados en la sección "Requerimientos previos" han sido satisfechos.

```
Ejemplo de código
```

?. Pruebe que el sistema está operando como se espera.

## 🚀 Despliegue <a name="despliegue"></a>

Esta sección describe las consideraciones importantes para un despliegue en producción exitoso.

## 🤹 Uso <a name="uso"></a>

Esta sección describe y demuestra usos básicos del sistema. Para detalles más específicos, consulte la documentación del proyecto.

## 📜 Registro de cambios <a name="registro_cambios"></a>

Todos los cambios importantes serán documentados en el archivo `CHANGELOG.md`. De clic [aquí](./CHANGELOG.md) para acceder a este archivo.

El formato de dicho archivo se basa en [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

Este proyecto se adiere al estándar [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 🗺️ Plan de desarrollo <a name="plan_desarrollo"></a>

## ✅ Pendientes <a name="pendientes"></a>

Esta sección registra algunos pendientes del proyecto. Para mayor información sobre el estado actual del proceso de desarrollo, consulte el archivo `TODO.md`, de clic [aquí](./TODO.md) para acceder a dicho archivo.

Siguiendo las mejores prácticas de desarrollo, los cambios deberían registrarse en el sistema de gestión de proyectos, o código, seleccionado para el proyecto.

## 🔨 Tecnologías utilizadas <a name="tecnologias_utilizadas"></a>

- [MongoDB](https://www.mongodb.com/) - Base de datos
- [Express](https://expressjs.com/) - Backend
- [VueJs](https://vuejs.org/) - Frontend

## ⚙️ Contribuciones <a name="contribuciones"></a>

Colaboraciones de desarrolladores, voluntarios para pruebas y traductores son siempre bienvenidas. Para más información sobre como contribuir con el proyecto se debe consultar el archivo `CONTRIBUTING.md`. De clic [aquí](./CONTRIBUTING.md) para acceder a dicho archivo.

## ✍️ Créditos <a name="creditos"></a>

- [Nombre](#enlacealperfil) por sus contribuciones en *DETALLE_CONTRIBUCION*

## 🤲 Soporte <a name="soporte"></a>

Por ahora la mejor forma de darle soporte al proyecto es compartiéndolo con otras personas.

## ⚖️ Licencia <a name="licencia"></a>

Este proyecto está licenciado bajo la licencia [LICENSE](./LICENSE). Para más información consulte el archivo `LICENSE.md`.
