<div align="center">
<a href="" rel="noopener">

<img width=200px height=200px src="https://i.imgur.com/6wj0hh6.jpg" alt="Project logo"></a>

# Project Title

*The only home we've ever known a still more glorious dawn awaits two ghostly white figures in coveralls and helmets are soflty dancing trillion permanence of the stars consciousness.*

[![Status](https://img.shields.io/badge/status-active-success.svg)]() 
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](./LICENSE)

---

<a href="#about">🧐 About</a> •
<a href="#getting_started">🏁 Getting Started</a> •
<a href="#deployment">🚀 Deployment</a> •
<a href="#usage">🤹 Usage</a> •
<a href="#changelog">📜 Changelog</a> •
<a href="#roadmap">🗺️ Roadmap</a> •
<a href="#todo">✅ Todo</a> •
<a href="#built_using">🔨 Built Using</a> •
<a href="#contributing">⚙️ Contributing</a> •
<a href="#credits">✍️ Credits</a> •
<a href="#support">🤲 Support</a> •
<a href="#license">⚖️ License</a>

![Screenshot](https://via.placeholder.com/720x360.png?text=Screenshot+or+Video+to+Showcase+your+Project)

</div>

## 🧐 About <a name="about"></a>

Write about 1-2 paragraphs describing the purpose of your project.

## 🏁 Getting Started <a name="getting_started"></a>

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See [deployment](#deployment) for notes on how to deploy the project on a live system.

### 🦺 Prerequisites

What things you need to install the software and how to install them.

```
Give examples
```

### 📦 Installing

A step by step series of examples that tell you how to get a development env running.

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo.

## 🚀 Deployment <a name="deployment"></a>

Add additional notes about how to deploy this on a live system.

## 🤹 Usage <a name="usage"></a>

Add notes about how to use the system.

## 📜 Changelog <a name="changelog"></a>

All notable changes to this project will be documented in `CHANGELOG.md`. Click [here](./CHANGELOG.md) to take a look.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 🗺️ Roadmap <a name="roadmap"></a>

## ✅ Todo <a name="todo"></a>

Pending tasks will be tracked in `TODO.md`. Click [here](./TODO.md) to take a look.

## 🔨 Built Using <a name="built_using"></a>

- [MongoDB](https://www.mongodb.com/) - Database
- [Express](https://expressjs.com/) - Server Framework
- [VueJs](https://vuejs.org/) - Web Framework
- [NodeJs](https://nodejs.org/en/) - Server Environment

## ⚙️ Contributing <a name="contributing"></a>

Developers, Testers and Translators are all welcomed. For more information on how to contribute to this project please refer to `CONTRIBUTING.md`. Click [here](./CONTRIBUTING.md) to take a look.

## ✍️ Credits <a name="credits"></a>

- [Name](#linktohisprofile) for his *CONTRIBUTION*

See also the list of [contributors](#) who participated in this project.

## 🤲 Support <a name="support"></a>

For now, just by sharing and/or staring this repository you are supporting my work.

## ⚖️ License <a name="license"></a>

This code is released under the [LICENSE](./LICENSE) license. For more information refer to `LICENSE.md`
